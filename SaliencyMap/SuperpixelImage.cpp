#include "stdafx.h"
#include "SuperpixelImage.h"

RNG rng(12345);

const int dcdThresh = 15;
const int alpha = 1.3;

SuperpixelImage::SuperpixelImage(string imageUrl, int supCount, int compactness, Mat& image)
{
	PictureHandler picHand;
	UINT* img = NULL;
	picHand.GetPictureBuffer(imageUrl, img, width_, height_);

	SLIC slic;
	int *labels = new int[width_ * height_];
	numLabels_ = 0;
	slic.DoSuperpixelSegmentation_ForGivenNumberOfSuperpixels(img, width_, height_, labels, numLabels_, supCount, compactness);

	image.copyTo(originalImage_);

	createSuperpixelImage(labels);

	computeSuperpixelDissimilarity();

	createSaliencyImage();

	delete labels;
	delete img;
};

void SuperpixelImage::createSuperpixelImage(int *labels)
{
	Mat Labels(height_, width_, CV_32SC1, labels);

	Mat SaveIm16;
	Labels.convertTo(superpixelImage_, CV_32FC1);

	Mat thres1, thres2, supMask;
	for(int i = 0; i < numLabels_; i++)
	{
		threshold(superpixelImage_, thres1, i - 1, 255, 1);
		threshold(superpixelImage_, thres2, i, 255, 1);

		compare(thres1, thres2, supMask, CMP_NE);

		superpixels.push_back(Superpixel(supMask, originalImage_));
	}

};

double SuperpixelImage::computeDCDColor(Superpixel &sup1, Superpixel &sup2)
{
	double acc(0);

	int clusterSize = sup1.getClusterSize();
	for(int i = 0; i < clusterSize; i++)
	{
		double p1 = sup1.getFractionOfPixels(i);
		for (int j = 0; j < clusterSize; j++)
		{
			double p2, del, euclDistance, colSim;
			p2 = sup2.getFractionOfPixels(j);
			del = (1 - abs(p1 - p2))*min(p1, p2);

			euclDistance = norm(sup1.getDominantColors(i), sup2.getDominantColors(j), NORM_L2);

			if (euclDistance <= dcdThresh)
				colSim = 1 - (euclDistance / dcdThresh*alpha);
			else 
				colSim = 0;

			acc += del * colSim;
		}
	}

	return 1 - acc;
}

void SuperpixelImage::computeSuperpixelDissimilarity()
{
	double DCDColor, euclDistance, acc(0);
	for (int i = 0; i < numLabels_; i++)
	{
		for (int j = 0; j < numLabels_; j++)
		{
			DCDColor = computeDCDColor(superpixels[i], superpixels[j]);
			euclDistance = norm(superpixels[i].getSuperpixelCenter() - superpixels[j].getSuperpixelCenter());
			acc += DCDColor / (1 + euclDistance);
		}

		superpixels[i].setSaliencyValue(acc / numLabels_);
		acc = 0;
	}
}

void SuperpixelImage::createSaliencyImage()
{
	Mat saliencyMap = Mat::zeros(height_, width_, CV_32FC1);
	for(int y = 0; y < height_; y++)
	{
		for(int x = 0; x < width_; x++)
		{
			int idx =  (int) superpixelImage_.at<float>(y,x);
			float saliency = superpixels[idx].getSaliencyValue();
			saliencyMap.at<float>(y,x) = saliency;
		}   
	}

	normalize(saliencyMap, saliencyImage_, 0, 1, NORM_MINMAX, -1);
}