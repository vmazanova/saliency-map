#include "stdafx.h"
#include "Superpixel.h"

Superpixel::Superpixel(Mat supMask, Mat originalImage)
{
	clusterCount_ = 3;
	supMask_ = supMask;

	Mat originalImage32FC3, cieLabImage32FC3;
	originalImage.convertTo(originalImage32FC3, CV_32FC3);
	originalImage32FC3 *= 1./255;
	cvtColor(originalImage32FC3, cieLabImage32FC3, CV_BGR2Lab);

	createSuperpixelCenter();

	computeDominantColors(cieLabImage32FC3);
}

void Superpixel::createSuperpixelCenter()
{
	Moments m = moments(supMask_, true);
	supCenter_ = Point(m.m10/m.m00, m.m01/m.m00);
}

void Superpixel::computeFractionOfPixels(Mat labels)
{
	fractionOfPixels_ = vector<double>(clusterCount_, 0);
	for(int i = 0; i < size_; i++)
	{
		int clusterIdx = labels.at<int>(i);
		fractionOfPixels_[clusterIdx]++;
	}

	for(int i = 0; i < clusterCount_; i++)
		fractionOfPixels_[i] /= size_;
}

Rect Superpixel::getBoundingRect()
{
	vector<vector<Point>> contours;
	findContours(supMask_, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE); 

	vector<Point> mergedContours;
	int extContoursSize = contours.size();
	for (int i = 0; i < extContoursSize; i++)
	{
		int intContoursSize = contours[i].size();
		for (int j = 0; j < intContoursSize; j++)
		{
			mergedContours.push_back(contours[i][j]);
		}
	}

	return boundingRect(mergedContours);
}

vector<Vec3f> Superpixel::computeSuperpixelPoints(Mat cieLabImage32FC3)
{
	int xMax, yMax;
	Rect boundRect;
	boundRect = getBoundingRect();
	xMax = boundRect.x + boundRect.width;
	yMax = boundRect.y + boundRect.height;

	vector<Vec3f> points;
	for(int y = boundRect.y; y <= yMax; y++)
	{
		for(int x = boundRect.x; x <= xMax; x++)
		{
			if (supMask_.at<uchar>(y,x) != 0) 
				points.push_back(cieLabImage32FC3.at<Vec3f>(y,x));
		}   
	}

	size_ = points.size();

	return points;
}

void Superpixel::computeDominantColors(Mat cieLabImage32FC3)
{
	vector<Vec3f> points = computeSuperpixelPoints(cieLabImage32FC3);

	Mat labels, centers(clusterCount_, 1, cieLabImage32FC3.type());
	kmeans(points, clusterCount_, labels, TermCriteria( CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 10, 1.0), 3, KMEANS_PP_CENTERS, centers);

	for(int i = 0; i < clusterCount_; i++)
	{
		Vec3f color;
		color[0] = centers.at<float>(i,0);
		color[1] = centers.at<float>(i,1);
		color[2] = centers.at<float>(i,2);
		dominantColors_.push_back(color);
	}

	computeFractionOfPixels(labels);
}