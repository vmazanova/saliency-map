﻿#include "stdafx.h"
#include "SuperpixelImage.h"
#include "Comparison.h"
#include <windows.h>
#include "Shlwapi.h"

using namespace cv;
using namespace std;

const int numberOfResolutions = 5;
const int superpixelSizes[numberOfResolutions][2] = {{50,10}, {100,10}, {200, 10}, {300, 10}, {500, 10}};

bool min(float x, float y) {
	return x < y;
}

bool max(float x, float y) {
	return x > y;
}

float findExtremesInMatrices(vector<SuperpixelImage> &superpixels, int x, int y, bool (*f)(float, float))
{
	float extreme = superpixels[0].getSaliencyImage().at<float>(y,x);
	for(int i = 1; i < numberOfResolutions; i++)
	{
		float currValue = superpixels[i].getSaliencyImage().at<float>(y,x);
		if ((*f)(currValue, extreme))
			extreme = currValue;
	}
	return extreme;
}

Mat createMultiResolutionSaliencyImage(Mat avgSuperpixelImage, vector<SuperpixelImage> &superpixels)
{
	Mat multiResSaliency = Mat::zeros(avgSuperpixelImage.rows, avgSuperpixelImage.cols, CV_32FC1);
	for (int y = 0; y < avgSuperpixelImage.rows; y++)
	{
		for (int x = 0; x < avgSuperpixelImage.cols; x++)
		{
			if (avgSuperpixelImage.at<float>(y, x) < 0.3)
				multiResSaliency.at<float>(y, x) = findExtremesInMatrices(superpixels, x, y, min);
			else
				multiResSaliency.at<float>(y, x) = findExtremesInMatrices(superpixels, x, y, max);
		}
	}

	return multiResSaliency;
}

Mat adaptiveRankedHistogram(Mat saliencyMap)
{
	int bins = 26;
	float range[] = { 0, 1.000001 };
	const float* histRange = { range };
	Mat hist;
	calcHist(&saliencyMap, 1, 0, Mat(), hist, 1, &bins, &histRange, true, false);

	Scalar histogramSum = sum(hist);
	hist /= histogramSum[0];

	int valleyPoint = 5;
	for (int i = 0; i < bins - 2; i++)
	{
		if (hist.at<float>(i) < 0.02 && hist.at<float>(i + 1) < 0.02 && hist.at<float>(i + 2) < 0.02)
		{
			valleyPoint = i;
			break;
		}
	}

	float threshValue =  1.0 / 26.0 * (valleyPoint + 1);
	threshold(saliencyMap, saliencyMap, threshValue, 1, THRESH_TOZERO);

	return saliencyMap;
}

string getFileName(const string& s) {

	char sep = '/';

	size_t i = s.rfind(sep, s.length());
	if (i != string::npos) {
		return(s.substr(i + 1, s.length() - i - 5));
	}

	return("");
}

int main(int argc, char* argv[])
{
	vector<SuperpixelImage> superpixels;
	string imageUrl = argv[1];
	Mat originalImage = imread(imageUrl, 1);
	
	Mat avgSuperpixelImage = Mat::zeros(originalImage.rows, originalImage.cols, CV_32FC1);
	for(int i = 0; i < numberOfResolutions; i++)
	{
		superpixels.push_back(SuperpixelImage(imageUrl, superpixelSizes[i][0], superpixelSizes[i][1], originalImage));

		avgSuperpixelImage += superpixels[i].getSaliencyImage();
	}
	avgSuperpixelImage /= numberOfResolutions;

	imshow("average", avgSuperpixelImage);

	Mat multiResSaliency = createMultiResolutionSaliencyImage(avgSuperpixelImage, superpixels);

	imshow("Multi Resolution Saliency", multiResSaliency);

	Scalar meanColor = mean(multiResSaliency);

	Mat suppressedSaliency;
	threshold(multiResSaliency, suppressedSaliency, meanColor[0], 1, THRESH_TOZERO);

	imshow("Suppressed Saliency", suppressedSaliency);

	Mat histogramNormalisedSaliency = adaptiveRankedHistogram(multiResSaliency);

	imshow("Histogram Normalised Saliency", histogramNormalisedSaliency);
	waitKey(0);
	
	PrecisonRecall(histogramNormalisedSaliency, getFileName(argv[1]));

	return 0;
}