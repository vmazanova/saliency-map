#ifndef SUPERPIXELIMAGE_H
#define SUPERPIXELIMAGE_H

#include <windows.h>
#include <iostream>
#include <string>
#include "PictureHandler.h"
#include "SLIC.h"
#include "Superpixel.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <omp.h>

using namespace cv;
using namespace std;


class SuperpixelImage {
	public:
		SuperpixelImage(string, int, int, Mat &);

		inline int getHeight(){ return height_; };
		inline int getWidth(){ return width_; };
		inline Mat getSaliencyImage(){ return saliencyImage_; };

	private:
		int numLabels_;
		int width_;
		int height_;

		Mat superpixelImage_;
		Mat originalImage_;
		Mat saliencyImage_;

		vector<Superpixel> superpixels;

		void createSuperpixelImage(int *);
		double computeDCDColor(Superpixel &, Superpixel &);
		void computeSuperpixelDissimilarity();
		void createSaliencyImage();
};

#endif