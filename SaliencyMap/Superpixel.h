#ifndef SUPERPIXEL_H
#define SUPERPIXEL_H

#include <windows.h>
#include <iostream>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace std;

class Superpixel {
	public:
		Superpixel(Mat, Mat);

		inline cv::Point getSuperpixelCenter(){ return supCenter_; };
		inline cv::Vec3f getDominantColors(int i){ return dominantColors_[i]; };
		inline double getFractionOfPixels(int i){ return fractionOfPixels_[i]; };
		inline int getClusterSize(){return clusterCount_; };

		inline void setSaliencyValue(double saliency){ saliency_ = saliency; };
		inline float getSaliencyValue(){return saliency_; };

	private:
		cv::Point supCenter_;
		int size_;
		int clusterCount_;
		std::vector<cv::Vec3f> dominantColors_;
		std::vector<double> fractionOfPixels_;
		float saliency_;

		Mat supMask_;

		void createSuperpixelCenter();
		void createHistogram(Mat);
		void computeFractionOfPixels(Mat);
		cv::Rect getBoundingRect();
		std::vector<cv::Vec3f> computeSuperpixelPoints(Mat);
		void computeDominantColors(Mat);
};

#endif