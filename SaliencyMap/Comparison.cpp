#include "StdAfx.h"
#include "Comparison.h"

string gt_path = "C:/Users/Nika/Pictures/dataset/truth/";
string saliency_path = "C:/Users/Nika/Pictures/dataset/saliency/";
string precision_recall_path = "C:/Users/Nika/Pictures/dataset/precision&recall/";

void PrecisonRecall(Mat image, string& filename)
{
	int sz = image.rows * image.cols;

	//create a path for the image
	gt_path.append(filename);
	gt_path.append(".bmp");

	//load a ground truth image and convert it to grayscale
	Mat ground_truth = imread(gt_path, 1);
	cvtColor(ground_truth, ground_truth, CV_BGR2GRAY);

	double min, max;
	minMaxLoc(image, &min, &max);
	image.convertTo(image, CV_8UC1, 255.0 / (max - min), -255.0 * min / (max - min));

	saliency_path.append(filename);
	saliency_path.append(".bmp");
	imwrite(saliency_path, image);

	//file to store results
	ofstream myfile;

	//create a path for a file
	precision_recall_path.append(filename);
	precision_recall_path.append(".txt");

	myfile.open(precision_recall_path, std::ios_base::app);

	int hit(0),sf(0), gf(0);
	float precision,recall;

	//loop through all the thresholds
	for(int j = 0; j < 255; j++)
	{
		int thresh = j;
		//compare each pixel from the saliency map to the ground truth
		for(int i = 0; i < sz; i++)
		{
			int y = i / image.cols;
			int x = i % image.cols;

			int pix_sal = image.at<uchar>(y, x);
			int pix_ground = ground_truth.at<uchar>(y, x);

			if( pix_sal > thresh)
			{
				sf++;		
				if(pix_ground != 0)
					hit++;
			}

			if(pix_ground != 0)
				gf++;

		}

		precision = float(hit)/float(sf);
		recall = float(hit)/ float(gf);

		myfile << precision << " ";
		myfile << recall << "\n";

		hit = 0;
		gf = 0;
		sf = 0;
	}
	
	ground_truth.release();
	myfile.close();
}